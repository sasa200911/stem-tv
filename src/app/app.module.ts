import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SimpleTimer} from 'ng2-simple-timer';
import { SlideshowModule } from 'ng-simple-slideshow';

import { AppComponent } from './app.component';
import { CurrentNextComponent } from './current-next/current-next.component';
import { CalendarEventsService } from './calendar-events.service';
import { UpcomingEventsComponent } from './upcoming-events/upcoming-events.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CarouselImagesService } from './carousel-images.service';

@NgModule({
  declarations: [
    AppComponent,
    UpcomingEventsComponent,
    CurrentNextComponent,
    CarouselComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SlideshowModule
  ],
  providers: [
    CalendarEventsService,
    SimpleTimer,
    CarouselImagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

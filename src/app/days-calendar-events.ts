import { CalendarEvent } from './calendar-event';

export class DaysCalendarEvents
{
  constructor(public date: string, public events: Array<CalendarEvent>) {}

}

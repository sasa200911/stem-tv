import { CalendarEvent } from './calendar-event';

export class GoogleCalendarEvents
{
  public items: Array<CalendarEvent>;
}
